package com.sand.test.base;

import groovy.util.ConfigObject;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import com.sand.base.FrameworkConfig;
import com.sand.base.driver.WebDriverFactory;

public abstract class BaseTestNGTest {
		
    protected static WebDriver driver;
    protected ConfigObject config = FrameworkConfig.getInstance().config;
  
	@BeforeClass
	public void beforeClass() {
		// create a WebDriver instance on the basis of the settings
		// provided in Config.groovy class
		driver = WebDriverFactory.getInstance().getDriver("local");
	}

	@BeforeMethod(alwaysRun = true)
	public void beforeMethod() {
		loadApplication();
	}
	
	protected void loadApplication() {
		driver.get((String) config.get("url"));
	}

	@AfterMethod(alwaysRun = true)
	public void deleteAllCookies() {
		driver.manage().deleteAllCookies();
	}
	
	@AfterClass(alwaysRun=true)
	public void afterClass() {
		driver.quit();
	}
}
