package com.sand.base.driver

import groovy.transform.Synchronized

import org.openqa.selenium.WebDriver


public final class WebDriverFactory {
	private static WebDriverFactory instance
	
	private WebDriverFactory() {}
	
	@Synchronized
	public static WebDriverFactory getInstance() {
		if (instance == null) {
			instance = new WebDriverFactory()
		}
		return instance
	}
	
	public WebDriver getDriver(String driverType) {
		switch (driverType) {
			case 'local':
				return new LocalDriver().createDriver()
			case 'remote':
				return new RemoteDriver().createDriver()
				break
			case 'mobile':
				return new MobileDriver().createDriver()
				break
			case 'saucelabs':
				return new SauceLabsDriver().createDriver()
				break
			default: throw new Exception ("UnSupported driver type requested: ${driverType}")
		}
	}
}